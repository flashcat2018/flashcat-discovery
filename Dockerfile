FROM adoptopenjdk/openjdk11

ADD ./target/discovery-1.0.jar /app/app.jar
CMD java -jar /app/app.jar 

EXPOSE 8888 8761